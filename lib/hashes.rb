# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_length = Hash.new
  str.split.each { |word| word_length[word] = word.length }
  word_length
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |k, v| v }[-1].first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k, v|
    older[k] = v
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letter_count = Hash.new(0)
  word.downcase.chars.each { |char| letter_count[char] += 1 }
  letter_count
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  arr_counts = Hash.new(0)
  arr.each { |el| arr_counts[el] = true }
  arr_counts.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  even_and_odd_count = {:even=>0, :odd=>0}
  numbers.each do |el|
    if el.even?
      even_and_odd_count[:even] += 1
    else
      even_and_odd_count[:odd] += 1
    end
  end
  even_and_odd_count
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = "aeiou"
  vowel_count = Hash.new(0)
  string.downcase.chars.each do |char|
    if vowels.include?(char)
      vowel_count[char] += 1
    end
  end
  max_count = vowel_count.select { |k, v| v == vowel_count.values.max }
  max_count.sort_by { |k, v| k }.first.first
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  student_pairs = []
  fall_and_winter_students = students.select { |k, v| v > 6 }.keys
  (0...fall_and_winter_students.length-1).each do |idx1|
    (idx1+1..fall_and_winter_students.length-1).each do |idx2|
      student_pairs << [fall_and_winter_students[idx1], fall_and_winter_students[idx2]]
    end
  end
  student_pairs.sort
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  populations = Hash.new(0)
  specimens.each { |specimen| populations[specimen] += 1 }
  number_of_species = populations.count
  smallest_population_size = populations.values.min
  largest_population_size = populations.values.max
  number_of_species**2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_characters = character_count(normal_sign)
  vandalized_characters = character_count(vandalized_sign)
  vandalized_characters.all?{ |k, v| normal_characters[k] >= v }
end

def character_count(str)
  characters = Hash.new(0)
  str.delete!(",.;:!? ")
  str.downcase.chars.each { |char| characters[char] += 1 }
  characters
end
